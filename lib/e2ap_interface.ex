defmodule E2APInterface do
  @moduledoc """
  Elixir interface for the E2AP serialization.
  """

  @spec enc_e2_setup_request(
          {any, [{any, integer, :ignore | :notify | :reject, list | integer | tuple}]},
          [bitstring]
        ) :: {bitstring | maybe_improper_list(any, bitstring | []), non_neg_integer}
  def enc_e2_setup_request(val, tag \\ [<<48>>]) do
    :E2AP.enc_E2setupRequest(val, tag)
  end

  @doc """
  Serialize a subscription request.

  Typespec synthesized by Dialyzer.

  Successfully serialize the following:

      iex> E2APInterface.enc_subscription_request({1, [{2, 5, :notify, 42}]}, ["hello"])

  Don't know what it means though.
  """
  @spec enc_subscription_request(
          {any,                                        # As far as I can tell, this value isn't used
           [                                           # This is a list of IEs ("information elements"; see table 9.1.1.1)
             {any,                                     # Unused
             integer,                                  # Probably a type id
             :ignore | :notify | :reject,              # Criticality
             integer | {any, any} | {any, any, any}}   # Value itself? Looks like the 2- and 3-tuples are most commonly used?
           ]},
          [bitstring]           # This is quite possibly the trigger definition
        ) :: {bitstring | maybe_improper_list(any, bitstring | []), non_neg_integer}
  def enc_subscription_request(arg1, arg2) do
    :E2AP.enc_RICsubscriptionRequest(arg1, arg2)
  end

  @spec dec_subscription_request(any, list) ::
          {:RICsubscriptionRequest,
           [{:"ProtocolIE-Field", char, :ignore | :notify | :reject, any}]}
  def dec_subscription_request(arg1, arg2) do
    :E2AP.dec_RICsubscriptionRequest(arg1, arg2)
  end

  def ident() do
    enc_subscription_request({1, [{2, 5, :notify, 42}]}, ["hello"])
    |> IO.inspect(label: "encoded")
    |> elem(1)
    # |> then(&dec_subscription_request(&1, ["hello", <<160>>, "0"]))
  end
end
