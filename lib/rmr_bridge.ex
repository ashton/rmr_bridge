defmodule RmrBridge do
  use GenServer

  @moduledoc """
  GenServer to manage a port to an RMR interface.

  Fires up a port to listen on and a port to receive from. Requires a "handler"
  Elixir process to which received messages of the form
  `{:data, #<port_pid>, term}` will be sent.

  The C send handler needs to implement the following:

  `{:send, type, data}` Send binary `data` over the RMR system with message type `type`.
  """

  ##############################################################################
  ##
  ##   Client Implementation
  ##
  ##############################################################################

  @doc """
  Default starup routine for sending/receving on an RMR system.
  """
  def startup(handler) do
    # I need to put the RMR routing table in the RMR_SEED_RT variable.
    {:ok, pid} =
      GenServer.start_link(__MODULE__, %{
        sender: "c_src/sender",
        listener: "c_src/listener",
        handler: handler
      })

    {:ok, pid}
  end

  @doc """
  Reconnect a closed port
  """
  def reconnect(bridge, sender, listener, handler) do
    GenServer.call(bridge, {:reconnect, sender, listener, handler})
  end

  def send_msg(bridge, message) do
    GenServer.call(bridge, {:send, 0, message})
  end

  def shutdown(bridge) do
    GenServer.call(bridge, :shutdown)
  end

  # Test/demo of sending
  def full_send(msg) do
    System.put_env("RMR_SEED_RT", "/tmp/basic.rt")
    {:ok, pid} = startup(self())
    IO.puts("\n\rSleeping to let the receiver wake up…")
    Process.sleep(3 * 1000)
    send_msg(pid, msg)
    pid
  end


  ##############################################################################
  ##
  ##   Server Implementation
  ##
  ##############################################################################

  @doc """
  Spawn a sending and listening ports and give the GenServer the PID of a
  process waiting to handle incomming messages.
  """
  @impl true
  def init(%{sender: send_ext, listener: recv_ext, handler: handler_pid})
      when is_pid(handler_pid) do
    port1 = Port.open({:spawn, send_ext}, [{:packet, 2}, :binary])
    port2 = Port.open({:spawn, recv_ext}, [{:packet, 2}, :binary])
    {:ok, %{sender: port1, listener: port2, handler: handler_pid}}
  end

  @impl true
  def handle_call({:reconnect, sender, listener, handler}, _from, %{closed: true}) do
    port1 = Port.open({:spawn, sender}, [{:packet, 2}, :binary])
    port2 = Port.open({:spawn, listener}, [{:packet, 2}, :binary])
    {:ok, %{sender: port1, listener: port2, handler: handler}}
  end

  def handle_call({:reconnect, _sender, _listener, _handler}, _from, _open_state),
    do: {:error, "Port still open"}

  def handle_call(:shutdown, _from, %{listener: l, sender: s}) do
    {:os_pid, pid_l} = Port.info(l, :os_pid)
    {:os_pid, pid_s} = Port.info(s, :os_pid)

	Port.close(l)
    Port.close(s)

    # Cleanup proccesses
    System.cmd("kill", [to_string(pid_l)])
    System.cmd("kill", [to_string(pid_s)])

    {:reply, :ok, %{closed: true}}
  end

  # Send commands require an open port
  def handle_call({:send, _, _}, _from, %{closed: true} = state) do
	{:reply, {:error, "Ports closed"}, state}
  end

  def handle_call({:send, type, data}, from, state) when is_binary(data),
    do: handle_call({:send, type, String.to_charlist(data)}, from, state)

  def handle_call({:send, type, data}, _from, %{sender: port} = state) when is_list(data) do
    IO.inspect(type, label: "[ex] type")
    IO.inspect(data, label: "[ex] data")
    term = :erlang.term_to_binary({:send, type, data})
    send(port, {self(), {:command, term}})
    {:reply, :ok, state}
  end

  @impl true
  def handle_info({port, {:data, data}}, %{listener: p, handler: h} = state) when port == p do
    IO.inspect(:erlang.binary_to_term(data), label: "data from listener")
    send(h, {:data, port, :erlang.binary_to_term(data)})
    {:noreply, state}
  end

  def handle_info({port, {:data, data}}, %{sender: s} = state) when port == s do
    IO.inspect(:erlang.binary_to_term(data), label: "\n\rGot response from sender:")
    {:noreply, state}
  end

  # :asn1ct.compile()
end
