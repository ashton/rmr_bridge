defmodule Complex2 do
  def start_port() do
    # This "packet" part tells Erlang to proceed data with its length
    Port.open({:spawn, "c_src/extprg"}, [{:packet, 2}, :binary])
  end

  def foo(port, x) do
    term = :erlang.term_to_binary({:foo, x})

    send(port, {self(), {:command, term}})

    receive do
	  {_cmplx, {:data, dat}} ->
        :erlang.binary_to_term(dat)

      otherwise ->
        {:otherwise, otherwise}
    after
      3000 ->
        IO.puts("No response in three seconds")
    end
  end
end
