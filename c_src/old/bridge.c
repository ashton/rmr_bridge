/* #include <string.h> */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
/* #include <pthread.h> */
/* #include <sys/socket.h> */
/* #include <arpa/inet.h> */
/* #include <pthread.h> */
/* #include <erl_driver.h> */
/* #include <ei.h> */
#include "bridge.h"

char in_buf[1024];

void start_echo() {
  // fprintf(stderr, "echo program started\n");
  int len;
  scanf("%d", &len);
  // fprintf(stderr, "ready to read %d bytes\n", len);

  if (len > 1024) {
    exit(1);                    // Error: capacity exceeded
  }

  /* Now read len bytes */
  read(0, in_buf, len);

  // fprintf(stderr, "reading done\n");

  printf("Received: '%s'\n", in_buf);
}

