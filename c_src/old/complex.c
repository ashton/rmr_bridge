#include <stdio.h>

int foo(int x) {
  fprintf(stderr, "\n\rFoo got called with %d\n\r", x);
  return x+1;
}

int bar(int y) {
  return y*2;
}
