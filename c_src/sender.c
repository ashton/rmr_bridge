#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <time.h>

/* RMR stuff */
#include <rmr/rmr.h>

/* Erlang Interface */
#include "ei.h"

/* See erl_comm.c */
typedef unsigned char byte;

int read_cmd(byte *buf);
int write_cmd(byte *buf, int len);

static void fail(int place) {
  fprintf(stderr, "[sender] Something went wrong %d\n", place);
  exit(1);
}

int send_it(void* mrc, rmr_mbuf_t* sbuf, rmr_mbuf_t* rbuf, int mtype, char* payload);

int main( int argc, char** argv ) {

  fprintf(stderr, "\n\rSender starting up; routing table: %s\n\r", getenv("RMR_SEED_RT"));

  void*              mrc;              // msg router context
  struct epoll_event events[1];        // list of events to give to epoll
  struct epoll_event epe;              // event definition for event to listen to
  int                ep_fd       = -1; // epoll's file des (given to epoll_wait)
  int                rcv_fd;           // file des for epoll checks
  int                nready;           // number of events ready for receive
  int                count       = 0;
  int                rcvd_count  = 0;
  char*              listen_port = "43086";
  int                delay       = 1000000; // mu-sec delay between messages
  int                mtype       = 0;
  int                stats_freq  = 100;

  if( argc > 1 ) {                    // simplistic arg picking
    listen_port = argv[1];
  }
  if( argc > 2 ) {
    delay = atoi( argv[2] );
  }
  if( argc > 3 ) {
    mtype = atoi( argv[3] );
  }

  if( (mrc = rmr_init( listen_port, 1400, RMRFL_NONE )) == NULL ) {
    fprintf( stderr, "unable to initialise RMR\n" );
    exit( 1 );
  }

  rcv_fd = rmr_get_rcvfd( mrc );  // set up epoll things, start by getting the FD from RMR
  if( rcv_fd < 0 ) {
    fprintf( stderr, "unable to set up polling fd\n" );
    exit( 1 );
  }
  if( (ep_fd = epoll_create1( 0 )) < 0 ) {
    fprintf( stderr, "[FAIL] unable to create epoll fd: %d\n", errno );
    exit( 1 );
  }
  epe.events = EPOLLIN;
  epe.data.fd = rcv_fd;

  if( epoll_ctl( ep_fd, EPOLL_CTL_ADD, rcv_fd, &epe ) != 0 )  {
    fprintf( stderr, "[FAIL] epoll_ctl status not 0 : %s\n", strerror( errno ) );
    exit( 1 );
  }

  rmr_mbuf_t* sbuf;             // send buffer
  rmr_mbuf_t* rbuf;             // received buffer

  sbuf = rmr_alloc_msg( mrc, 256 );    // alloc 1st send buf; subsequent bufs alloc on send
  rbuf = NULL;                         // don't need to alloc receive buffer

  while( 1 ) {                         // send messages until the cows come home

    // Decode the erlang term
    byte buf[100];
    char body_buff[1024];
    int index = 0;
    int version = 0;
    int arity = 0;
    char ctl_atom[128];
    long mtype_long = 0;
    int res = 0;
    ei_x_buff res_buf;
    ei_init();
    while (read_cmd(buf) > 0) {
      if (ei_decode_version(buf, &index, &version) != 0)
        fail(1);
      if (ei_decode_tuple_header(buf, &index, &arity) != 0)
        fail(2);
      if (arity != 3)
        fail(3);
      if (ei_decode_atom(buf, &index, ctl_atom) != 0)
        fail(4);
      if (ei_decode_long(buf, &index, &mtype_long) != 0)
        fail(5);

      int type_key = 0;
      int thing_size = 0;
      if (ei_get_type(buf, &index, &type_key, &thing_size) != 0) { fail(54); }

      if (ei_decode_string(buf, &index, body_buff) != 0)
        fail(55);

      // Now we can dispatch on the tuples
      if (strncmp(ctl_atom, "send", 3) == 0) {
        res = send_it(mrc, sbuf, rbuf, (int)mtype_long, body_buff);
      }
      // Attempt to encode the result
      if (ei_x_new_with_version(&res_buf) != 0)
        fail(6);
      if (ei_x_encode_long(&res_buf, res) != 0)
        fail(7);
      write_cmd(res_buf.buff, res_buf.index);

      if (ei_x_free(&res_buf) != 0)
        fail(8);
      index = 0;
    }
  }
}

int send_it(void* mrc, rmr_mbuf_t* sbuf, rmr_mbuf_t* rbuf, int mtype, char* payload) {
  while( ! rmr_ready( mrc ) ) {        // must have route table
    sleep( 1 );                        // wait til we get one
  }
  fprintf( stderr, "\n\r[sender] rmr is ready\n\r" );

  // Could probably use `strcpy` here
  snprintf(sbuf->payload, 200, "Message: %s", payload);

  sbuf->mtype = mtype;          // fill in the message bits
  sbuf->len   = strlen( sbuf->payload ) + 1; // send full ascii-z string
  sbuf->state = 0;
  sbuf        = rmr_send_msg( mrc, sbuf ); // send & get next buf to fill in
  while( sbuf->state == RMR_ERR_RETRY ) { // soft failure (device busy?) retry
    sbuf = rmr_send_msg( mrc, sbuf );     // w/ simple spin that doesn't give up
  }
  fprintf( stderr, "\n\r[sender] message sent type %d\n\r", mtype );
  return 0;
}

