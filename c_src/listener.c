#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* RMR stuff */
#include <rmr/rmr.h>

/* Erlang Interface */
#include "ei.h"

/* See erl_comm.c */
typedef unsigned char byte;

int read_cmd(byte *buf);
int write_cmd(byte *buf, int len);

static void fail(int place) {
  fprintf(stderr, "[listener] Something went wrong %d\n", place);
  exit(1);
}

int main( int argc, char** argv ) {

  fprintf(stderr, "\n\rListener starting up; routing table: %s\n\r", getenv("RMR_SEED_RT"));

  void* mrc;                       // msg router context
  long long total = 0;
  rmr_mbuf_t* msg = NULL;          // message received
  int stat_freq = 10;              // write stats after reciving this many messages
  int i;
  char*    listen_port = "4560";   // default to what has become the standard RMR port

  ei_x_buff res_buf;
  int res = 0;                     // Index into result buffer

  if( argc > 1 ) {
    listen_port = argv[1];
  }
  if( argc > 2 ) {
    stat_freq = atoi( argv[2] );
  }
  /* fprintf( stderr, "listening on port: %s\n", listen_port ); */

  mrc = rmr_init( listen_port, RMR_MAX_RCV_BYTES, RMRFL_NONE );
  if( mrc == NULL ) {
    fprintf( stderr, "ABORT:  unable to initialise RMr\\n" );
    exit( 1 );
  }

  while( ! rmr_ready( mrc ) ) {    // wait for RMR to get a route table
    /* fprintf( stderr, "waiting for ready\n" ); */
    sleep( 1 );
  }
  fprintf( stderr, "\n\r\n[listener] rmr now shows ready\n\r" );

  while( 1 ) {                     // receive until killed
    msg = rmr_rcv_msg( mrc, msg ); // block until one arrives

    if( msg ) {
      if( msg->state == RMR_OK ) {
        if (ei_x_new_with_version(&res_buf) != 0)
          fail(1);
        if (ei_x_encode_tuple_header(&res_buf, 2) != 0)
          fail(2);
        if (ei_x_encode_atom(&res_buf, "data") != 0)
          fail(3);
        // Send len-1 bytes; Erlang won't need the null terminator
        if (ei_x_encode_binary(&res_buf, msg->payload, msg->len-1) != 0)
          fail(4);

        write_cmd(res_buf.buff, res_buf.index);

        if (ei_x_free(&res_buf) != 0)
          fail(8);
      }
    }
  }
}
