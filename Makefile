SERVER	:= pc612

.PHONY: full-setup setup
full-setup: setup compile asn1_compile

setup:
	sudo bash setup.sh

.PHONY: copy
copy:
	rsync -aivzh --delete --exclude-from=.gitignore --exclude=.git --exclude=.elixir_ls . ashton@${SERVER}.emulab.net:~/rmr_bridge/

.PHONY: compile asn1_compile
compile:
	cd c_src && make all

asn1_compile:
	cd asn1 && make
	mkdir -p src
	cp asn1/*.erl src/
	cp asn1/*.hrl src/
