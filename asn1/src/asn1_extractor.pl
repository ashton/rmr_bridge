#!/usr/bin/env perl
use strict;
use warnings;
use feature 'say';
use feature 'signatures';
use autodie;                    # "It is better to die() rather return() in failure" -- Klingon programming proverb

use File::Basename;
use File::Copy;

# You can view the documentation for this file with `perldoc pipeline.pl'

=pod

=head1 NAME

O-RAN ASN.1 Extraction Pipeline

=head1 SYNOPSIS

  perl asn1_extractor.pl O-RAN.WG3.E2AP-v02.03.docx

=head1 DESCRIPTION

Because someone thought it would be a good idea to distribute these critical
files as Word documents, we have to have this silly little pipeline to get the
code we need. I mean, really.

=head2 Prerequisites

You will need L<Pandoc|https://pandoc.org> installed on your system and available in C<PATH>.

=head2 Running

Should be pretty simple: pass a list of C<.docx> files to this script. It will
use Pandoc to convert them to plain text, and then will clean up the Pandoc
output and strip out the ASN.1 bits at put them in their proper files.

Note that this does the job of the old C<extract_asn1_from_spec.pl> script.
(It's a little snazzier since we use the stateful flip-flop operator instead of
manually tracking our state while looping through the file.) This script also
tries to automatically guess the name of the ASN.1 file it should dump the
fragments into based off of what is in the fragment. See source code for
details.

=head1 INTERNALS

What follows is documentation for individual functions.

=cut

foreach my $docx_file (@ARGV) {
    my $base = basename($docx_file, ".docx");
    say "Working on $docx_file → $base.asn1...";
    report_and_run("pandoc $docx_file -o $base.txt");
    my @files = extract_asn1("$base.txt", $base);
    say "Working on $docx_file → " . join(', ', @files) . "...done\n";
}

=pod

=head2 C<report_and_run>

Given a string command, print it out, then call L<system> on it.

=cut

sub report_and_run($cmd) {
    say "---> $cmd";
    system($cmd);
}

=pod

=head2 C<extract_asn1($txt_file, $base)>

Takes a file C<$txt_file> and extracts all the ASN.1 blocks embedded in it,
delimited with C<ASN1START>..C<ASN1STOP>. Fragments will be written to
C<$base_fragno.asn1>, and then this script will attempt to rename them based off
of the first header that it sees.

Also performs some sanitation:

=over 4

=item *

Removes backslashes left over from Pandoc conversion.

=item *

Replaces non-breaking spaces with regular ones. (That was a fun bug to find.)

=item *

Tidies up miscellaneous other issues.

=back

=cut


sub extract_asn1($txt_file, $base) {
    # Step 1: extract blocks
    open my $in, '<', $txt_file;
    my $frag_no = 0;
    my @frag_files = ();

    my $out;
    my $newname = '';

    while (<$in>) {
        # New fragment
        if (/-- ASN1START/) {
            $frag_no++;
            $newname = '';
            open $out, '>', "${base}_frag$frag_no.asn1"; # Open new one
            say "Writing to fragment $frag_no...";
        }
        # Within the fragment
        if (/-- ASN1START/../-- ASN1STOP/) {
            # Guess the name of this fragment from the first declaration we see
            $newname = $1 if ! $newname && /^([a-zA-Z0-9-]+) \{$/;

            s/\\//g;            # Remove escapes introduced by Pandoc
            tr/ / /;            # Remove non-breaking spaces

            chomp if /^--/;     # for comments, don't print out a new line
            if (/^\*+$/) {
                chomp;
                print $out " ";
            }
            if (/^#+ (.*)$/) {  # drop funny pound signs
                $_ = $1;
            }
            print $out $_;
        }

        if (/-- ASN1STOP/) {
            close $out if defined $out;
            if ($newname) {
                report_and_run("mv ${base}_frag$frag_no.asn1 $newname.asn1");
                push @frag_files, "$newname.asn1";
            }
            else {
                say "WARNING: Not able to guess name of fragment $frag_no";
                push @frag_files, "${base}_frag$frag_no.asn1";
            }
        }
    }
    return @frag_files;
}

=pod

=head1 AUTHOR

Ashton Wiersdorf

=cut
