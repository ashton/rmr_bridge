#!/usr/bin/bash

# You will need to run this with sudo

echo "################################################################################"
echo "##                        UPDATING PACKAGES                                   ##"
echo "################################################################################"

apt-get update
# apt-get upgrade -y
apt-get install -y cmake g++ libssl-dev rapidjson-dev git ca-certificates curl gnupg apt-transport-https apt-utils pkg-config autoconf libtool software-properties-common lsb-release

# Get an up-to-date version of Pandoc
pushd /tmp
wget https://github.com/jgm/pandoc/releases/download/2.19.2/pandoc-2.19.2-1-amd64.deb
sudo dpkg -i pandoc-2.19.2-1-amd64.deb

echo "################################################################################"
echo "##                    INSTALLING ELIXIR & ERLANG                              ##"
echo "################################################################################"

# Source: https://computingforgeeks.com/how-to-install-latest-erlang-on-ubuntu-linux/
curl -fsSL https://packages.erlang-solutions.com/ubuntu/erlang_solutions.asc | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/erlang.gpg
echo "deb https://packages.erlang-solutions.com/ubuntu $(lsb_release -cs) contrib" | sudo tee /etc/apt/sources.list.d/erlang.list
sudo apt-get update
sudo apt-get install -y erlang
sudo apt-get install -y elixir

echo "################################################################################"
echo "##                       FETCHING RMR PREREQS                                 ##"
echo "################################################################################"

curl -s https://packagecloud.io/install/repositories/o-ran-sc/release/script.deb.sh | os=debian dist=stretch sudo bash
apt-get install -y mdclog mdclog-dev rmr rmr-dev
ldconfig

rm -rf /var/lib/apt/lists/*

# Build the RMR library

echo "################################################################################"
echo "##                       BUILDING RMR LIBRARY                                 ##"
echo "################################################################################"

cd ~
git clone https://gitlab.flux.utah.edu/powderrenewpublic/xapp-frame-cpp
cd xapp-frame-cpp
mkdir -p build && cd build
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DDEV_PKG=1 ..
make && make install && ldconfig
cd /tmp && rm -rf /tmp/xapp-frame-cpp

# cd /tmp
# wget https://packages.erlang-solutions.com/erlang-solutions_2.0_all.deb
# apt-get install -y erlang-solutions_2.0_all.deb
# apt-get update
# apt-get install -y elixir erlang
