# RMR Bridge

Use the E2 interface with Elixir

## Synopsis

All examples assume the setup script has been run. This will install Erlang, Elixir, and the necessary RMR system libraries.

    $ sudo bash setup.sh

### RMR Demo

Compile the `sender` and `listener` binaries that Elixir uses over a port connection:

    $ make compile

For the RMR example to work, you will need to copy `c_src/basic.rt` to `/tmp/basic.rt` or adjust the `RMR_SEED_RT` environment variable appropriately. See the [RMR documentation](https://docs.o-ran-sc.org/projects/o-ran-sc-ric-plt-lib-rmr/en/latest/rt_tables.html) for more information about what this routing table is.

Fire up Elixir and start playing around:

    $ mix deps.get
    $ iex -S mix

    iex> my_port = RmrBridge.full_send("hello, world")
    iex> flush()
    {:data, #Port<0.8>, {:data, "Message: hello, world"}}
    iex> RmrBridge.send_msg(my_port, "hello again")
    iex> flush()
    {:data, #Port<0.8>, {:data, "Message: hello again"}}

### ASN.1 Serialization

The Erlang ASN.1 serialization/deserialization files have been included in this repository. You should be able to just run `iex -S mix` without any problems.

Should you ever need to rederive them from the ASN.1 spec, you will first need the ASN.1 files. 

Get the `.docx` that describes them (stupid that we have to do this, I know) and save to `asn1/src/O-RAN.WG3.E2AP-v02.03.docx`. (You can see the Makefile in the `asn1` directory for details on renaming this/updating the version.)

Once that file is in place, run from the root directory:

    $ make asn1_compile

You will need [Pandoc](https://pandoc.org) installed on your system to make that work. See the [ASN.1 Recovery](#asn1-recovery) section.

Now you can fire up Elixir and start playing around:

    $ mix deps.get
    $ iex -S mix

    iex> E2APInterface.enc_subscription_request({1, [{2, 5, :notify, 42}]}, ["hello"])

## Description

This library takes a stab at making communication over the E2 interface easy from Elixir.

The library comes has two major modules:

 - serializing/deserializing messages compliant to the E2AP interface (`E2APInterface`)
 - sending bytes over the RMR library

Use these modules in tandem to communicate with the RAN.

Additionally, there are a bunch of generated Erlang files that perform the actual serialization/deserialization. These are generated from ASN.1, which describes the data structures used in E2.

### `E2APInterface`

This module abstracts the serialization of the E2AP "service models" or semantically interesting messages sent over the E2 interface. If you want to, for example, send a RIC subscription request, a function here will help you take the subscription data and turn it into bytes that you'd then send along the network.

#### Status

We're not sure how to use these serialization functions properly. I (Ashton) don't have enough domain knowledge to recover the semantic role of the functions. This will take a bit of work and a fair amount of reverse-engineering.

### `RmrBridge`

Once you have some bytes, you can use the RMR Bridge module to actually send the bytes over the network realized by the RMR library. RMR is a C library; we use an [Elixir Port](https://hexdocs.pm/elixir/Port.html) to talk to a C binary. The C binary acts as a little bit of glue between Elixir and the rest of the existing RMR tech. It can serialize and deserialize Erlang terms, a feature used by the `RMRBridge` module to control the message metadata. It can also send and receive messages from the RMR system.

#### Architecture

![Architecture diagram](./docs/arch.png)

There are two C programs (`sender.c` and `listener.c`, found in the [c_src](./c_src) directory) that are needed for this to work.

When `RmrBridge.startup/1` gets called, it creates two OS processes, each one running `sender` and `listener` respectively. Inside the BEAM, two Port processes start up to manage communication with these external programs.

The `startup/1` procedure takes the `PID` of process that handles incoming messages from the RMR network. This process will get messages of the form `{:data, #<Port PID>, term()}` when something arrives from the `listener`.

#### Status

We have a simple ping-pong example working. There's a whole lot more that the RMR components need in order to be fully functional. (For starters, our binaries need to be able to handle dynamic route table updates. This should be straight-forward, though it will be a little bit of work.)

## ASN.1 Recovery

For some stupid reason, the ASN.1 files that we need for this project get distributed as Microsoft Word documents. We have an extractor script that automates the process of getting the raw ASN.1 that we need out of the Word documents. It's fully automatic and doesn't require you to have Word installed.

You do need the following:

 - Recent-ish version of Perl (anything newer than 5.30 should be just fine)
 - [Pandoc](https://pandoc.org/) (version 2.14.* or better)

Run `perldoc asn1/src/asn1_extractor.pl` for more details on how to use the script.

### Installing Pandoc

**WARNING:** Do not use the version that's in the Debian repository! It's too old.

Follow the [installation instructions](https://pandoc.org/installing.html#linux) to get an up-to-date version.

(**Note:** the setup script tries to install Pandoc for you if you're on Cloudlab. Use these instructions if you're working elsewhere.)

## Terms

 - `E2 interface`
 
   This is the specification of how the RIC communicates with the RAN. Think of it as an alternative to HTTP. This interface defines the shape of different categories of *service models* that you can use.
   
 - `Service model`
 
   Teleco's name for an API endpoint. I dunno why they call it that, but here we are.

 - `RMR`
 
   This is a library that we can use to send bytes between different peers using RMR. This is what we use to send messages to the right things in the RIC/RAN space.

 - `ASN.1`
 
   ASN.1 is a data structure serialization spec. It's kind of like Google's Protobufs. We take the E2AP spec expressed as ASN.1 files, and run Erlang's ASN.1 compiler, which gives us a whole bunch of Erlang functions we can call to automatically serialize/deserialize the data structures we care about when sending E2AP messages.

## Author

Ashton Wiersdorf
