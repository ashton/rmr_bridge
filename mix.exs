defmodule RmrBridge.MixProject do
  use Mix.Project

  def project do
    [
      app: :rmr_bridge,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # XML building and parsing
      # {:xml_builder, "~> 2.1"},
      # {:sweet_xml, "~> 0.7.3"},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false}
    ]
  end
end
